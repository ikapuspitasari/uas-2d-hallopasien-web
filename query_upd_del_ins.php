<?php
    $DB_NAME = "rumahsakit";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
                $no_hp = $_POST["no_hp"];
                $nama_pasien = mysqli_real_escape_string($conn,trim($_POST["nama_pasien"]));
                $nama_kamar = $_POST["nama_kamar"];
                $kelamin = $_POST["kelamin"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";

                $sql = "SELECT id_kamar from kamar where nama_kamar='$nama_kamar'";
                $sql1 = "SELECT id_jk from jenis_kelamin where kelamin='$kelamin'";

                $result = mysqli_query($conn,$sql);

                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_kamar = $data['id_kamar'];
                    $id_jk = $data1['id_jk'];

                    $sql = "INSERT into pasien(no_hp, nama_pasien, id_kamar, photos, alamat, id_jk) values(
                        '$no_hp','$nama_pasien','$id_kamar','$file', '$alamat', '$id_jk')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $sql = "delete from pasien where no_hp='$no_hp'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            echo json_encode($respon); exit(); //insert data sukses semua
                        }
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
            case "update":
                $no_hp = $_POST["no_hp"];
                $nama_pasien = mysqli_real_escape_string($conn,trim($_POST["nama_pasien"]));
                $nama_kamar = $_POST["nama_kamar"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";
                $kelamin = $_POST["kelamin"];


                $sql = "SELECT id_kamar from kamar where nama_kamar='$nama_kamar'";
                $sql1 = "SELECT id_jk from jenis_kelamin where kelamin='$kelamin'";

                $result = mysqli_query($conn,$sql);
                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_kamar = $data['id_kamar'];
                    $id_jk = $data1['id_jk'];

                    $sql = "";
                    if($imstr==""){
                        $sql = "UPDATE pasien SET nama_pasien='$nama_pasien',id_kamar=$id_kamar, alamat='$alamat', id_jk='$id_jk'
                        where no_hp='$no_hp'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }else{
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            $sql = "UPDATE pasien SET nama_pasien='$nama_pasien',id_kamar=$id_kamar,photos='$file', alamat='$alamat', id_jk='$id_jk'
                                    where no_hp='$no_hp'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit(); //update data sukses semua
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }
                    }
                }
            break;
            case "delete":
                $no_hp = $_POST["no_hp"];
                $sql = "SELECT photos from pasien where no_hp='$no_hp'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }
                    $sql = "DELETE from pasien where no_hp='$no_hp'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>