<?php
$DB_NAME = "rumahsakit";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT p.no_hp, p.nama_pasien, p.alamat, jk.kelamin, k.nama_kamar, p.photos
            FROM pasien p, kamar k, jenis_kelamin jk
            WHERE p.id_kamar = k.id_kamar AND p.id_jk = jk.id_jk";
    
    $result = mysqli_query($conn,$sql);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>RSUD ISKAK</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="pasien.php">Data Pasien</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="kamar.php">Data Kamar</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
        <h1> Data Pasien </h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped bg-light">
                <thead class="table-striped table-grey" style="text-align: center;">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">No Hp</th>
                        <th scope="col">Nama Pasien</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Kamar</th>
                        <th scope="col">Photo</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    while($pas = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?></th>
                        <td><?php echo $pas['no_hp']; ?></td>
                        <td><?php echo $pas['nama_pasien']; ?></td>
                        <td><?php echo $pas['kelamin']; ?></td>
                        <td><?php echo $pas['alamat']; ?></td>
                        <td><?php echo $pas['nama_kamar']; ?></td>
                        <td style="text-align: center;">
                            <img src="images/<?php echo $pas['photos']; ?>" style="width: 200px;" alt="Foto Pasien">
                        </td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>

</body>

</html>